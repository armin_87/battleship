 
; batalla naval 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h
msj db 10,13," MAR JUGADOR 1 || MAR JUGADOR 2 ","$" ,10,13  
    mov ah,9
    mov dx,offset msj     ;muestra en pantalla msj
    int 21h       
      
mov dx, offset new_line       ;salta una linea
    mov ah, 09h
    int 21h 

jmp start

new_line db 10,13, '$'   ;inicializo una linea en blanco

width db 32              ;dimension de los mares
height db 11

board1 db "***************||***************" 
;variables estructura oculta
botes_jug1 db "*#####*********"
           db "***************"
           db "***************"
           db "###************"
           db "*********##****"
           db "***************"
           db "***************"
           db "***************"
           db "*####**********"
           db "***************"
           db "*********######"
              
botes_jug2 db "*********######"
           db "***************"
           db "***************"
           db "**##***********"
           db "***************"
           db "***********###*"
           db "***************"
           db "**####*********"
           db "***************"
           db "***************"
           db "**********#####" 
;variables para centrar el cursor
x_centerSea1 db 7
x_centerSea2 db 24
y_centerSea  db 7           
;variables para limitar los mares
x_leftSea2 db 17             
x_leftSea1 db 0  
y_upSea  db 2
;variables para ubicar los contadores de puntos
x_contador1 db 7
y_contador  db 16
x_contador2 db 24 
;variables para el uso del contador
puntos1u db 0
d1 db 0
u1 db 0
puntos2u db 0
d2 db 0 
u2 db 0

limiteSea2r: 
      mov dl, x_leftSea2
      jmp SetCursor

limiteSea1r: 
      mov dl, x_leftSea1
      jmp SetCursor
                          
limiteSeaU: 
      mov dh, y_upSea
      jmp SetCursor

start:
    mov bx, offset board1
    call print_board
 
msjf db 10,13,"  PTS. JUG.1:  ||  PTS. JUG.2:    ","$", 10,13  
    mov ah,9                  ;imprimo en pantalla mensaje
    mov dx,offset msjf
    int 21h 
        
    mov dx, offset new_line   ;salto linea
    mov ah, 09h
    int 21h
    
mar2:
    mov dh ,y_centerSea  ;centro cursor en mar 2
    mov dl, x_centerSea2
    mov bh,0
    mov ah,2
    int 10h
getKey:
    mov ah, 07h         ;pide ingresar tecla
    int 21h
    call Key_validator
    jmp getKey
ret


proc print_board
    mov al, 00h
    mov ah,2
    int 10h
    xor cx,cx
    mov cl, height
  ptr_lineCount:
    xor SI,SI
    push cx
    mov cl, width
    mov ah,02h
  ptr_line:
    mov dl, [BX+SI]
    inc SI
    int 21h
    loop ptr_line
    mov dx, offset new_line
    mov ah, 09h
    int 21h
    pop cx
    loop ptr_linecount
ret
Endp print_board
    
proc Key_validator    
                        
        cmp al, 's'     ;Compara que tecla se presiono, si es 's'    
        je Down
                
        cmp al, 'w'     ;Compara que tecla se presiono, si es 'w'    
        je Up
                
        cmp al, 'a'     ;Compara que tecla se presiono, si es 'a'    
        je Left
                
        cmp al, 'd'     ;Compara que tecla se presiono, si es 'd'    
        je Right
                
        cmp al, 'f'    ;Compara que tecla se presiono, si es 'f'    
        je fire
                
        jmp endKeyValidator        ;Sino, no hace nada y vuelve a pedir una tecla
fire:    
        push dx
         call estructura_oculta
        pop dx 
        cmp dl,17 
         ja mar1
         jmp mar2 
         
Right:  
        cmp dl,1Fh
        je limiteSea2r
        cmp dl,0Eh                   
        je limiteSea1r
        
        inc dl              ;para reposicionar el cursor columna
        jmp SetCursor      ;llamo al procedimiento para setear cursor
Left:
        cmp dl,11h
        je limiteSea2r
        cmp dl,00h
        je limiteSea1r
        
        dec dl              ;para reposicionar el cursor columna
        jmp SetCursor      ;llamo al procedimiento para setear cursor
Up:
        cmp dh,02h
        je limiteSeaU
        
        dec dh              ;para reposicionar el cursor fila
        jmp SetCursor      ;llamo al procedimiento para setear cursor
Down:
        cmp dh,0Ch
        je limiteSeaU
        
        inc dh              ;para reposicionar el cursor fila
        jmp SetCursor      ;llamo al procedimiento para setear cursor
SetCursor:              
        mov ah, 02h
        mov bh, 00
        int 10h             ;Analizar esta int, que es la que posiciona el cursor
        endKeyValidator:
ret
   
  mar1:
    mov dh ,y_centerSea
    mov dl, x_centerSea1   ;centro cursor en mar1
    mov bh,0
    mov ah,2 
    int 10h 
    jmp endKeyValidator 
endp Key_validator

proc estructura_oculta  
    cmp dl,17      ;compara si el mar es del jugador1 o jugador2 al ser 17 el medio
    jng barcosjug1 ;si no es mayor a 17 se carga los barcos del mar1  
    jg barcosjug2  ;si es mayor a 17 se cargan los barcos de mar2
  barcosjug1:
    sub dh, 2 ;esta recta se hace para quitar las 2 lineas de mas que tiene la grilla
    mov ah, 00 ;esto es para ver si quedo algun residuo en ah y lo borra
    mov al, dl ;copia lo que se encuentra en dl a al 
    mov si, ax ;se copia lo que esta en ax a si, esto se realiza asi, ya que no se puede pasar registro de 4bits a 8 bits 
    mov al, dh
    mov bl, 15 ;se multiplica dh por 15 al ser esta las cantidad de columna que se desplaza hacia abajo
    mul bl
    add si, ax ;se agrega el resultado de la multiplicacion a si para tener la posicion del cursor 
    mov bx, offset botes_jug1 ;se carga el indice de botes_jug1 en bx    
    cmp [bx+si],'#'; compara si  el disparo acerto o no 
    je  disparo_exitoso2  ;si acerto entra aqui
    jne disparo_fallido   ;si no etra aqui
  barcosjug2:
    sub dl, 17   ;se resta 17 a dl  al ser esta la suma entre el mar1 y las barras separadoras
    sub dh,2
    mov ah, 00
    mov al, dl
    mov si, ax
    mov al, dh
    mov bl, 15
    mul bl
    add si, ax
    mov bx, offset botes_jug2    
    cmp [bx+si],'#'
    je  disparo_exitoso1
    jne disparo_fallido
  
  disparo_fallido: 
        mov dl,'n'     ;si el disparo es fallido cambia a "n"                 
        mov ah,2
        int 21h
        ret
   disparo_exitoso1: 
        mov dl,'s'    ;si el disparo es exitoso cambia a "s"
        mov ah,2
        int 21h
        inc puntos1u  ;incrementa los puntos
        mov al, puntos1u
        aam              ;instruccion que sirve para separa los puntos en decena y unidades
        mov u1, al
        mov d1, ah
        cmp d1,2
        jmp contador1
        ret 
   disparo_exitoso2:
        mov dl,'s'     ;si el disparo es exitoso cambia a "s"
        mov ah,2
        int 21h  
        inc puntos2u
        mov al, puntos2u
        aam            ;instruccion que sirve para separa los puntos en decena y unidades
        mov u2, al
        mov d2, ah 
        
        jmp contador2
        ret 
endp estructura_oculta

  contador1: 
    mov dh ,y_contador
    mov dl, x_contador1  ;jugar donde se va imprimir el contador de puntos
    mov bh,0
    mov ah,2
    int 10h
            
    mov ah,02h                        
    mov dl,d1
    add dl,30h     ;muestra en pantalla los puntos decenas
    int 21h
    
    mov dl, u1     ;muestra en pantalla puntos unidades
    add dl,30h
    int 21h
    cmp d1, 2       ; compara si la decena llego a 2 que seria 20 puntos
    je finaljg1
    jmp setcursor

  contador2: 
    mov dh ,y_contador
    mov dl, x_contador2  ;jugar donde se va imprimir el contador de puntos
    mov bh,0
    mov ah,2
    int 10h  
    mov ah,02                        
    mov dl,d2      ;muestra en pantalla los puntos decenas
    add dl, 30h 
    int 21h
    
    mov dl,u2
    add dl,30h     ;muestra en pantalla puntos unidades
    int 21h
    cmp d2, 2       ; compara si la decena llego a 2 que seria 20 puntos
    je finaljg2
    jmp setcursor
    
   finaljg1:
    mov dh ,17  
    mov dl, 6
    mov bh,0
    mov ah,2
    int 10h
       
    mov ah,9                  ;imprimo en pantalla mensaje
    mov dx,offset msjg1
    int 21h 
                             
    msjg1 db 10,13,"  JUG1 ES EL GANADOR","$", 10,13
   ret
   
   finaljg2:
    mov dh ,17  ;mueve el cursor debajo 
    mov dl, 6
    mov bh,0
    mov ah,2
    int 10h
     
    mov ah,9                  ;imprimo en pantalla mensaje
    mov dx,offset msjg2
    int 21h  
                               
    msjg2 db 10,13,"JUG2 ES EL GANADOR","$", 10,13
   ret 